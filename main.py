import requests
import os
from dotenv import load_dotenv

load_dotenv()

apikey = os.getenv("APIKEY")

url = "https://api.meraki.com/api/v1/organizations"
payload_get_orgs = None
headers = {
    "Content-Type": "application/json",
    "Accept": "application/json",
    "X-Cisco-Meraki-API-Key": f"{apikey}"
}

get_organizations = requests.get(url, headers=headers, data = payload_get_orgs).json()

for organization in get_organizations:
    organization_id = organization["id"]
    saml_url = f"https://api.meraki.com/api/v1/organizations/{organization_id}/saml"
    payload_saml_enable = '''{ "enabled": true }'''
    requests.put(saml_url, headers=headers, data=payload_saml_enable)
    payload_saml_createidp = '''{
    "idpId": "ab0c1de23Fg",
    "x509certSha1Fingerprint": "insertcerthere",
    "sloLogoutUrl": ""
    }'''
    saml_url_createidp = f"https://api.meraki.com/api/v1/organizations/{organization_id}/saml/idps"
    requests.post(saml_url_createidp, headers=headers, data=payload_saml_createidp)
    saml_url_roles = f"https://api.meraki.com/api/v1/organizations/{organization_id}/samlRoles"
    payload_full = '''{
        "role": "meraki_write",
        "orgAccess": "full",
        "networks": [],
        "tags": []
        }'''
    payload_read = '''{
        "role": "meraki_read",
        "orgAccess": "read-only",
        "networks": [],
        "tags": []
        }'''
    requests.post(saml_url_roles, headers=headers, data=payload_full)
    requests.post(saml_url_roles, headers=headers, data=payload_read)
    print(f"Organization {organization['name']} has been updated")


